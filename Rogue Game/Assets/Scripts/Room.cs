using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Room : MonoBehaviour

{
//Reference for the door objects
[Header("Door Objects")]
public Transform northDoor;
public Transform eastDoor;
public Transform westDoor;
public Transform southDoor;
//Reference for the wall objects
[Header("Wall Objects")]
public Transform northWall;
public Transform eastWall;
public Transform westWall;
public Transform southWall;
//How many tiles are there in the room
[Header("Room Size")]
public int insideWidth;
public int insideHeight;
//Objects to instatiate
[Header("Room Prefabs")]
public GameObject enemyPrefab;
public GameObject coinPrefab;
public GameObject healthPrefab;
public GameObject keyPrefab;
public GameObject bonfirePrefab;
public GameObject exitdoorPrefab;
//List of positions to avoid instantiating new objects on top of old 
private List<Vector3> usedPositions = new List<Vector3>();

public void GenerateInterior()
{
    //Do we spawn enemies?
    if(Random.value < Generation.instance.enemySpawnChance)
    {
        SpawnPrefab(enemyPrefab, 1, Generation.instance.maxEnemiesPerRoom +1);
    }
    //Do we spawn coins?
    if(Random.value < Generation.instance.coinSpawnChance)
    {
        SpawnPrefab(coinPrefab, 1, Generation.instance.maxCoinsPerRoom +1);
    }
    //Do we spawn health?
    if(Random.value < Generation.instance.healthSpawnChance)
    {
        SpawnPrefab(healthPrefab, 1, Generation.instance.maxHealthPerRoom +1);
    }
    //Do we spawn bonfire(s)?
    if(Random.value < Generation.instance.bonfireSpawnChance)
    {
        SpawnPrefab(bonfirePrefab, 1, Generation.instance.maxBonfiresPerRoom +1);
    }
}
public void SpawnPrefab(GameObject prefab, int min = 0, int max = 0)
{
    int num = 1;
    if(min != 0 || max != 0)
        num = Random.Range(min, max);

    // for each of the prefabs
    for(int x = 0; x < num; x++)
     {
        //instantiate the prefab
        GameObject obj = Instantiate(prefab);
        //Get the nearest tile to a random position inside the room 
        Vector3 pos = transform.position + new Vector3(Random.Range(-insideHeight / 2, insideWidth /2 +1)*Generation.instance.tileSize, Random.Range(-insideHeight / 2, insideHeight /2 +1)* Generation.instance.tileSize, 0);
        //Place the prefab to the random position.
        obj.transform.position = pos;
        //Add the current position to the old 'usedPositions' list
        usedPositions.Add(pos);

        if(prefab == enemyPrefab)
            EnemyManager.instance.enemies.Add(obj.GetComponent<Enemy>()); //Add it to the EnemyManager enemies list
            
     }
}
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
